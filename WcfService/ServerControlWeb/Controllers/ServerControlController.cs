﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ServerControlWeb.ServiceReference1;

namespace ServerControlWeb.Controllers
{
    public class GrafikDeger
    {
        public List<int> Cpu_x;
        public List<int> Cpu_y;

        public List<int> Ram_x;
        public List<int> Ram_y;

        public List<int> Drive_x;
        public List<long> Drive_y;

        public List<string> Server_name;

        public GrafikDeger()
        {
            Cpu_x = new List<int>();
            Cpu_y = new List<int>();

            Ram_x = new List<int>();
            Ram_y = new List<int>();

            Drive_x = new List<int>();
            Drive_y = new List<long>();

            Server_name = new List<string>();
        }
    }

    public class Servertablo
    {
        public List<TableCpu> cpu = new List<TableCpu>();
        public List<TableRam> ram = new List<TableRam>();
        public List<TableDrive> drive = new List<TableDrive>();
    }

    public class ServerControlController : Controller
    {
        /// <summary>
        /// Anasayfa olarak kullanıyoruz ve bu action'da tableserver adlı tablomuzdan verileri çekip view'e basıyoruz
        /// </summary>
        /// <returns>
        /// sqlde kayıtlı olan TableServer adlı tablonun değerlerinin döndürür
        /// </returns>
        public ActionResult Servers()
        {
            Service1Client obj = new Service1Client(); //wcf servisimizden bir nesne oluşturduk
            return View(obj.GetServer());
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="yenisunucu">kullanıcı yeni sunucu eklemek isterse post işlemiyle ekleyecepi sunucuyu alıp veri tabanına ekliyoruz</param>
        /// <returns>
        /// TableServer tablosunun son halini view'e basıyoruz
        /// </returns>
        [HttpPost]
        public ActionResult Servers(string yenisunucu)
        {
            Service1Client obj = new Service1Client();
            obj.PutServerName(yenisunucu);//yeni sunucu adını wcf serviste bulunan fonksiyonla veritabanına kaydettik 
            return View(obj.GetServer());
        }

        /// <summary>
        /// bu kısım anasayfadan seçtiğimiz sunucuyla alakalı detaylı bilgileri tablolar halinde basan kısım
        /// </summary>
        /// <param name="str">Anasayfadan seçtiğimiz herhangi bir sunucunun ismi</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ServerResults(string str)
        {
            Servertablo tbl = new Servertablo(); 
            Service1Client obj = new Service1Client();

            //tabloları clear et
            tbl.cpu.Clear();
            tbl.ram.Clear();
            tbl.drive.Clear();

            //str(servername) adına göre veritabanından çekip tbl adlı class'ımıza dolduruyor
            //addrange kullanmamızın sebebi ise tekli veri değilde liste halinde veri ekleme yaptıgımız için
            tbl.cpu.AddRange(obj.GetCpu(str));
            tbl.ram.AddRange(obj.GetRam(str));
            tbl.drive.AddRange(obj.GetDrive(str));

            PartialView("PartialTablo", tbl);
            return View();
        }

        /// <summary>
        /// tablolandırma ekranında son kaç adet kaydı gösteri seçtiysek ona göre verileri çekip partialview'e yolluyor 
        /// </summary>
        /// <param name="sunucu">seçtiğimiz sunucu adı</param>
        /// <param name="secim">kaç adet kaydın gösterileceğine dair seçtiğimiz değer</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ServerResults(string sunucu, int secim)
        {
            Servertablo tbl = new Servertablo();
            Service1Client obj = new Service1Client();

            tbl.cpu.Clear();
            tbl.ram.Clear();
            tbl.drive.Clear();

            tbl.cpu.AddRange(obj.GetCpu_N(sunucu,secim));
            tbl.ram.AddRange(obj.GetRam_N(sunucu, secim));
            tbl.drive.AddRange(obj.GetDrive_N(sunucu, secim));

            PartialView("PartialTablo", tbl);
            return View();
        }

        /// <summary>
        /// tüm sunucuların grafikleri için gerekli verileri sql den wcf yardımıyla çekip modele basıyor.
        /// modeli partialview'e yollayarak grafikteki ilgili alanları dolduruyor
        /// </summary>
        /// <returns>partialview'e model yollayıp ilgili partialview'de ekrana basıyor.</returns>
        public ActionResult Grafikler()
        {
            // 
            Service1Client obj = new Service1Client();            
            var list_server = obj.GetServer();
            bool gönder = false;
            GrafikDeger notlist = new GrafikDeger(); //bir adet örneğive bu örneği dolduruyoruz alttaki listeye ekliyoruz
            GrafikDeger[] deger = new GrafikDeger[list_server.Count()];
            List<TableConf> konf = obj.GetConf().ToList<TableConf>();
            

            for (int index = 0; index < list_server.Count(); ++index)
            {
                var list_ram = obj.GetRam(list_server[index].ServerName);
                var list_cpu = obj.GetCpu(list_server[index].ServerName);
                var list_drive = obj.GetDrive(list_server[index].ServerName);

                string konu = "Bilgilendirme " + list_server[index].ServerName;//bilgilendirilecek server adını stringe atıyoruz
                string icerik = "";//mail body'sinin oldugu string
                string disk_uyarı = "";//sistemde bulunan disklerin her birinin doluluk oranına bakıp belli bir yüzdenin altına düşmüşse bu sstringe ekleme yapıyor.

                //ram bilgileri
                var size = list_ram.Length;
                for(int i = size - 5; i < size; ++i)
                {
                    notlist.Ram_x.Add(list_ram[i].RamId);
                    notlist.Ram_y.Add((int)list_ram[i].RamAvailableSpace);
                }

                //cpu bilgileri
                size = list_cpu.Length;
                for(int i=size-5; i < size; ++i)
                {
                    notlist.Cpu_x.Add(list_cpu[i].CpuId);
                    var temp = list_cpu[i].CpuUtility.Split(',');
                    notlist.Cpu_y.Add(Convert.ToInt32(temp[0]));
                }

                //kaç adet driver var onu buluyor
                List<string> drive_ad = new List<string>();
                drive_ad.Clear();
                string tmp = list_drive[0].DriveName;
                drive_ad.Add(tmp);
                size = 1;
                while (tmp != list_drive[size].DriveName)
                {
                    drive_ad.Add(list_drive[size].DriveName);
                    ++size;
                }

                //kaç adet disk varsa onların son degerlerini toplayıp toplam ne kadar alan olduğunu buluyor
                for(int i=0; i < drive_ad.Count(); ++i)
                {
                    int sz = 0;
                    var kontrol = obj.GetDriveByName(list_server[index].ServerName,drive_ad[i]);

                    //her bir diskin % lik bazda kullanılabilecek alanını hesaplıyoruz ve bu deger konfigurasyonda sectiğimiz degerin altına düşerse mail içerine hangi diskin yüzde kaç boş oldugunu yazıcak
                    if (kontrol[kontrol.Count() - 1].DriveTotalSize != 0)
                    {
                        double yüzde = (((double)kontrol[kontrol.Count() - 1].DriveAvailableSize/(1024*1024*1024)) / ((double)kontrol[kontrol.Count() - 1].DriveTotalSize/(1024 * 1024 * 1024)));
                        if (yüzde * 100 < konf[0].KonfDisk)
                        {
                            disk_uyarı += list_server[index].ServerName+" adlı sunucunun "+ drive_ad[i] + " adlı diskinin kullanım kapasitesi azalmıştır. Kullanılabilirlik alan yüzdesi %" + (yüzde * 100).ToString("n2")+"\n";
                        }
                    }

                    for (int j = kontrol.Length - 5; j < kontrol.Length ; ++j)
                    {
                        if( i > 0)
                        {
                            notlist.Drive_y[sz + index*5] += (long)kontrol[j].DriveAvailableSize /(1024*1024*1024);
                            ++sz;
                        }
                        else
                        {
                            notlist.Drive_y.Add((long)kontrol[j].DriveAvailableSize / (1024 * 1024 * 1024));
                        }
                    }
                }

                notlist.Server_name.Add(list_server[index].ServerName);
                deger[index] = notlist;
                
                if (notlist.Cpu_y.Sum() / notlist.Cpu_y.Count() >= konf[0].KonfCpu)
                {
                    icerik = "CPU Aşırı Yükleme % :" + notlist.Cpu_y.Sum() / notlist.Cpu_y.Count() + "\n";
                    gönder = true;
                }
                if(notlist.Ram_y.Sum() / notlist.Ram_y.Count() <= konf[0].KonfRam)
                {
                    icerik += "Ram Kullanım Uyarısı MB :" + notlist.Ram_y.Sum() / notlist.Ram_y.Count() + "\n";
                    gönder = true;
                }
                if (gönder)
                {
                    icerik += disk_uyarı;
                    obj.SendMail(konu, icerik);
                }
                    
                
            }
            PartialView("PartialGrafik", deger);
            return View();
        }

        /// <summary>
        /// konfigürasyon ayarlarının yaptığımız action.
        /// cpu,ram ve disk konfigürasyon ayarlarını view'e basıyor
        /// </summary>
        /// <returns></returns>
        public ActionResult Konfigurasyon()
        {
            Service1Client obj = new Service1Client();
            var data = obj.GetConf();
            return View(data);
        }

        /// <summary>
        /// kullanıcının belirlediği değerleri alıp veritabanına ekliyoruz. artık yeni konfigurasyon ayarlarımız bunlar oluyor.
        /// </summary>
        /// <param name="cpu">Cpu ayarı</param>
        /// <param name="ram">ram ayarı</param>
        /// <param name="disk">disk ayarı</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Konfigurasyon(string cpu,string ram,string disk)
        {
            Service1Client obj = new Service1Client();
            TableConf yeniKonf = new TableConf();
            yeniKonf.KonfCpu = float.Parse(cpu);
            yeniKonf.KonfRam = float.Parse(ram);
            yeniKonf.KonfDisk = float.Parse(disk);
            obj.PutConf(yeniKonf);
            var data = obj.GetConf();
            return View(data);
        }

    }
}